﻿using Microsoft.EntityFrameworkCore;
using moviecruiser.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace moviecruiser.Data.Persistence
{
    public interface IMoviesDbContext
    {
        DbSet<Movie> Movies { get; set; }
        int SaveChanges();
    }
}
