﻿using moviecruiser.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace moviecruiser.Data.Persistence
{
    public interface IMovieRepository
    {
        List<Movie> GetAllMovies();
        int UpdateMovieDetails(int id, string comment);
        int AddNewMovie(Movie movie);
        int DeleteMovieDetails(int id);
        Movie GetSingleMovieInfo(int id);
    }
}
