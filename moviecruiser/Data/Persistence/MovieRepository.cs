﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using moviecruiser.Data.Models;

namespace moviecruiser.Data.Persistence
{
    public class MovieRepository : IMovieRepository
    {
        private readonly IMoviesDbContext _context;
        public MovieRepository (IMoviesDbContext context)
        {
            _context = context;
        }

        //Get all movies
        public List<Movie> GetAllMovies()
        {
            return _context.Movies.ToList();
        }

        //Insert new movies
        public int AddNewMovie(Movie movie)
        {
            _context.Movies.Add(movie);
            var result = _context.SaveChanges();
            return result;

        }

        //Update movie comment
        public int UpdateMovieDetails(int id,string comment)
        {
            var movie = _context.Movies.First(x => x.Id == id);
            movie.Comments = comment;
            var updated = _context.SaveChanges();
            return updated;
        }

        //Delete a movie from DB
        public int DeleteMovieDetails(int movId)
        {
            var movie = _context.Movies.FirstOrDefault(x=>x.Id==movId);
            _context.Movies.Remove(movie);
             var result = _context.SaveChanges();
            return result;
        }
        //Get info about single movie movies
        public Movie GetSingleMovieInfo(int id)
        {
            return _context.Movies.First(x => x.Id == id);
        }
    }
}
