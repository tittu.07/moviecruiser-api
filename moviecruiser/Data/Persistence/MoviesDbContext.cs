﻿using Microsoft.EntityFrameworkCore;
using moviecruiser.Data.Models;
using System.Collections.Generic;

namespace moviecruiser.Data.Persistence
{
    public class MoviesDbContext : DbContext, IMoviesDbContext
    {
        public MoviesDbContext() { }

       public MoviesDbContext(DbContextOptions<MoviesDbContext> options) : base(options) { }

        public DbSet<Movie> Movies { get ; set ; }
    }
}

