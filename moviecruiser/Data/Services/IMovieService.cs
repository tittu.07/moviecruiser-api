﻿using moviecruiser.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace moviecruiser.Data.Services
{
    public interface IMovieService
    {
        List<Movie> GetAllMovies();
        int UpdateMovieDetails(int id,string comment);
        int AddNewMovie(Movie movie);
        int DeleteMovie(int id);
        string GetSingleMovieInfo(int id);
        string GetMoviesFromTmdb(string type,int pagenumber);
        string SearchMovie(string movieName, int pagenumber);
    }
}
