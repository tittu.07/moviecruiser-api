﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using moviecruiser.Data.Models;
using moviecruiser.Data.Persistence;

namespace moviecruiser.Data.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _repo;
        private readonly IConfiguration _config;
        private string tmdbBaseUrl ;
        private string apiKey;
        public MovieService(IMovieRepository repo, IConfiguration configuration)
        {
            _repo = repo;
            _config = configuration;
        }

        public List<Movie> GetAllMovies()
        {
           return _repo.GetAllMovies();
        }

        public int AddNewMovie(Movie movie)
        {
            return _repo.AddNewMovie(movie);
        }

        public int UpdateMovieDetails(int id, string comment)
        {
            return _repo.UpdateMovieDetails(id, comment);
        }
        public int DeleteMovie(int id)
        {
            return _repo.DeleteMovieDetails(id);
        }
       
        public string GetSingleMovieInfo(int id)
        {
            tmdbBaseUrl = _config["TmdbBaseUri"];
            apiKey = _config["ApiKey"];
            var URL = tmdbBaseUrl + "movie/" + id + "?" + apiKey ;
            var data = getVlauesFromTmdb(URL);
            return data;
        }
        public string GetMoviesFromTmdb(string type,int pagenumber)
        {
            tmdbBaseUrl = _config["TmdbBaseUri"];
            apiKey = _config["ApiKey"];
            var URL = tmdbBaseUrl + "movie/" + type + "?" + apiKey + "&page=" + pagenumber;
            var data = getVlauesFromTmdb(URL);
            return data;
        }
        public string SearchMovie(string movieName, int pagenumber)
        {
            tmdbBaseUrl = _config["TmdbBaseUri"];
            apiKey = _config["ApiKey"];
            var URL = tmdbBaseUrl + "search/movie?" + apiKey + "&query="+movieName + "&page=" + pagenumber;
            var data = getVlauesFromTmdb(URL);
            return data;
        }

        private string getVlauesFromTmdb(string url)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);


            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));


            HttpResponseMessage response = client.GetAsync(url).Result;
            var dataObjects = "";
            if (response.IsSuccessStatusCode)
            {                
                dataObjects = response.Content.ReadAsStringAsync().Result;
            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }
            client.Dispose();

            return dataObjects;
        }
    }
}
