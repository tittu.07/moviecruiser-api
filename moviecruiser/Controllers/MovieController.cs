﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using moviecruiser.Data.Models;
using moviecruiser.Data.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace moviecruiser.Controllers
{
    [EnableCors("CORS")]
    [Authorize]
    public class MovieController : Controller
    {
        private readonly IMovieService _service;

        public MovieController(IMovieService service)
        {
            _service = service;
        }
        /// <summary>
        /// To fetch all watchlisted movies information from application database
        /// </summary>
        /// <returns></returns>
        [Route("api/movie/allmovies")]
        [HttpGet]
        public IActionResult GetAllMovies()
        {
            try
            {
                var result = _service.GetAllMovies();
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To add a new movie information to application DB
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        [Route("api/movie/newmovie")]
        [HttpPost]
        public IActionResult AddNewMovie([FromBody]Movie movie)
        {
            try
            {
                var isAvailable = _service.GetAllMovies().FirstOrDefault(x => x.Id == movie.Id);
                if (isAvailable == null)
                {
                    var result = _service.AddNewMovie(movie);
                    return Ok(result);
                }
                else
                {
                    return Ok("Already added to watchlist");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To update movie details in application DB
        /// </summary>
        /// <param name="id"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        [Route("api/movie/moviedetails")]
        [HttpPost]
        public IActionResult UpdateMovieDetails([FromBody] Movie movie)
        {
            try
            {
                var result = _service.UpdateMovieDetails(movie.Id, movie.Comments);
                return Ok(result);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To delete a movie from application DB using movie ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/movie/movie")]
        [HttpPost]
        public IActionResult DeleteMovie([FromBody]Movie movie)
        {
            try
            {
                var result = _service.DeleteMovie(movie.Id);               
                return Ok(result);
               
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To fetch single movie info using the TMDB api
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/movie/singlemovieinfo/{id}")]
        [HttpGet]
        public IActionResult GetSingleMovieInfo(int id)
        {
            try
            {
                var isAvailable = _service.GetAllMovies().FirstOrDefault(x => x.Id == id);
                var comments = "";
                var isWatchlisted = false;
                if (isAvailable != null)
                {
                    isWatchlisted = true;
                    comments = isAvailable.Comments;
                }

                var result = _service.GetSingleMovieInfo(id);
                return Ok(new {Result = result,IsWatchlisted=isWatchlisted,Comments= comments });
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// To fetch the movie details using TMDB api
        /// </summary>
        /// <param name="type"></param>
        /// <param name="pagenumber"></param>
        /// <returns></returns>
        [Route("api/movie/moviesfromtmdb/{type}/{page}")]
        [HttpGet]
        public IActionResult GetMoviesFromTmdb(string type, int pagenumber = 1)
        {
            try
            {
                var result = _service.GetMoviesFromTmdb(type, pagenumber);
                return Ok(result);
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To search movies in TMDB using the inputed keyword
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pagenumber"></param>
        /// <returns></returns>
        [Route("api/movie/search/{keyword}/{page}")]
        [HttpGet]
        public IActionResult SearchMovie(string keyword, int pagenumber = 1)
        {
            try
            {
                var result = _service.SearchMovie(keyword, pagenumber);
                return Ok(result);
              
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
