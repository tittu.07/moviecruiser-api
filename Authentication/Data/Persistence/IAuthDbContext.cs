﻿using Authentication.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Authentication.Data.Persistence
{
    public interface IAuthDbContext
    {
        DbSet<User> Users { get; set; }
        int SaveChanges();
    }
}
