﻿using Authentication.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authentication.Data.Persistence
{
    
    public class UserRepository : IUserRepository
    {
        private readonly IAuthDbContext _context;

        public UserRepository(IAuthDbContext context)
        {
            _context = context;
        }

        public User Register(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();
            return user;
        }

        public User Login(string userName, string password)
        {
            var user = _context.Users.FirstOrDefault(x => x.UserName == userName && x.Password==password);
            return user;
        }

        public User FindUserByUserName (string userName)
        {
            var user = _context.Users.FirstOrDefault(x => x.UserName == userName);
            return user;
        }
    }
}
