﻿using Authentication.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authentication.Data.Persistence
{
    public interface IUserRepository
    {
        User Register(User user);
        User Login(string userName, string password);
        User FindUserByUserName(string userName);


    }
}
