﻿using Authentication.Data.Models;
using Microsoft.EntityFrameworkCore;


namespace Authentication.Data.Persistence
{
    public class AuthDbContext : DbContext, IAuthDbContext
    {
        public AuthDbContext() { }
        public AuthDbContext(DbContextOptions<AuthDbContext> options) : base(options) { }
        public DbSet<User> Users { get; set; }
    }
}
