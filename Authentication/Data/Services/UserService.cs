﻿using Authentication.Data.Models;
using Authentication.Data.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authentication.Data.Services
{
    public class UserService:IUserService
    {
        private readonly IUserRepository _repo;
        public UserService(IUserRepository repo)
        {
            _repo = repo;            
        }

        /// <summary>
        /// To authenticate a user
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public User Login(string UserName,string password)
        {
            var user = _repo.Login(UserName, password);           
            return user;          
        }
        /// <summary>
        /// To rergister a new user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public User Register(User user)
        {
            
                var regUser = _repo.Register(user);
                return regUser;
           
        }
        /// <summary>
        /// Check whether username is already exsist
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public bool IsExsist(string userName)
        {
            var isExsist = _repo.FindUserByUserName(userName);
            if (isExsist != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
