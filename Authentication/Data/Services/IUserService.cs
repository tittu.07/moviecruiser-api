﻿using Authentication.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authentication.Data.Services
{
    public interface IUserService
    {
        User Login(string UserName, string password);
        User Register(User user);
        bool IsExsist(string userName);

    }
}
