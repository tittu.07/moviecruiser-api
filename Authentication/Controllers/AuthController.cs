﻿using System;
using Authentication.Data.Models;
using Authentication.Data.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Authentication.Controllers
{

    // [Produces("application/json")]'
    [EnableCors("ECORS")]
    [Route("api/Auth")]
    
    public class AuthController : Controller
    {
        private readonly IUserService _service;
        private readonly ITokenGenerator _token;

        public AuthController(IUserService service,ITokenGenerator token)
        {
            _service = service;
            _token = token;
        }
        /// <summary>
        /// This is to validate the entered username and password
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [Route("login")]
        [HttpPost]
        public IActionResult Login([FromBody] User user)
        {
            try
            {

              var result = _service.Login(user.UserName, user.Password);

                if (result != null)
                {
                    string value = _token.GetJWTToken(user.UserName);
                    return Ok(value);
                }
                else
                {
                    return NotFound("Invalid credentials");
                }
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// This method is to register a new user to access the application
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [Route("register")]
        [HttpPost]
        public IActionResult Register([FromBody]User user)
        {
            try
            {
                var isExsist = _service.IsExsist(user.UserName);

                if (isExsist)
                {
                    return StatusCode(StatusCodes.Status302Found, "User alredy exsist");
                }
                else
                {
                    var result = _service.Register(user);
                    return Ok(new {Mesage= "Successfully registered"});
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}