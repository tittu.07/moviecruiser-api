﻿using Moq;
using moviecruiser.Data.Models;
using moviecruiser.Data.Persistence;
using moviecruiser.Data.Services;
using System;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using Xunit;

namespace test
{
    public class MovieServiceTest
    {
        IConfiguration configuration;

      
        [Fact]
        public void GetAllMovies()
        {
            //Arrange
            var mockRepo = new Mock<IMovieRepository>();
            mockRepo.Setup(repo => repo.GetAllMovies()).Returns(this.GetMovies());
            var service = new MovieService(mockRepo.Object, configuration);

            //Act
            var actual = service.GetAllMovies();

            //Assert
            Assert.IsAssignableFrom<List<Movie>>(actual);
            Assert.Equal(3, actual.Count);
        }

        [Fact]
        public void AddNewMovie()
        {
            var newMovie = new Movie { Id = 354443, Name = "AntMan", PosterPath = "Antman.jpg", ReleaseDate = "12-10-2012", Comments = String.Empty, VoteAverage = 7.8, VoteCount = 980 };

            //Arrange
            var mockRepo = new Mock<IMovieRepository>();
            mockRepo.Setup(repo => repo.AddNewMovie(newMovie)).Returns(1);
            var service = new MovieService(mockRepo.Object, configuration);

            //Act
            var actual = service.AddNewMovie(newMovie);

            //Assert
            Assert.Equal(1, actual);
        }

        [Fact]
        public void UpdateMovieDetails()
        {
            //Arrange
            var mockRepo = new Mock<IMovieRepository>();
            mockRepo.Setup(repo => repo.UpdateMovieDetails(354440, "Hello Superman")).Returns(1);
            var service = new MovieService(mockRepo.Object, configuration);

            //Act
            var actual = service.UpdateMovieDetails(354440, "Hello Superman");

            //Assert
            Assert.Equal(1, actual);
        }
        [Fact]
        public void DeleteMovieDetails()
        {
            //Arrange
            var mockRepo = new Mock<IMovieRepository>();
            mockRepo.Setup(repo => repo.DeleteMovieDetails(354440)).Returns(1);
            var service = new MovieService(mockRepo.Object, configuration);

            //Act
            var act = service.DeleteMovie(354440);

            //Assert
            Assert.Equal(1, act);


        }

        [Fact]
        public void negative_GetAllMovies()
        {
            //Arrange
            var mockRepo = new Mock<IMovieRepository>();
            List<Movie> movies = new List<Movie>();
            mockRepo.Setup(repo => repo.GetAllMovies()).Returns(movies);
            var service = new MovieService(mockRepo.Object, configuration);

            //Act
            var actual = service.GetAllMovies();

            //Assert
            Assert.Equal(movies, actual);
        }

        [Fact]
        public void negative_AddNewMovie()
        {
            //Arrange
            var mockRepo = new Mock<IMovieRepository>();
            mockRepo.Setup(repo => repo.AddNewMovie(null)).Returns(0);
            var service = new MovieService(mockRepo.Object, configuration);

            //Act
            var actual = service.AddNewMovie(null);

            //Assert
            Assert.Equal(0, actual);
        }

        [Fact]
        public void negative_UpdateMovieDetails()
        {
            //Arrange
            var mockRepo = new Mock<IMovieRepository>();
            mockRepo.Setup(repo => repo.UpdateMovieDetails(1290093456,null)).Returns(0);
            var service = new MovieService(mockRepo.Object, configuration);

            //Act
            var actual = service.UpdateMovieDetails(123456, null);

            //Assert
            Assert.Equal(0, actual);
        }
        [Fact]
        public void negative_DeleteMovieDetails()
        {
            //Arrange
            var mockRepo = new Mock<IMovieRepository>();
            mockRepo.Setup(repo => repo.DeleteMovieDetails(354440)).Returns(0);
            var service = new MovieService(mockRepo.Object, configuration);

            //Act
            var act = service.DeleteMovie(354440);

            //Assert
            Assert.Equal(0, act);


        }



        private List<Movie> GetMovies()
        {
            var movie = new List<Movie>();
            movie.Add(new Movie { Id = 354440, Name = "Superman", PosterPath = "superman.jpg", ReleaseDate = "12-10-2012", Comments = String.Empty, VoteAverage = 7.8, VoteCount = 980 });
            movie.Add(new Movie { Id = 354441, Name = "Anaconda", PosterPath = "anaconda.jpg", ReleaseDate = "12-10-2012", Comments = String.Empty, VoteAverage = 8.0, VoteCount = 1080 });
            movie.Add(new Movie { Id = 354442, Name = "Independence Day", PosterPath = "spiderman.jpg", ReleaseDate = "12-10-2012", Comments = String.Empty, VoteAverage = 7.8, VoteCount = 980 });

            return movie;
        }
    }
}
