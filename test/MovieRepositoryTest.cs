﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using moviecruiser.Data.Persistence;
using moviecruiser.Data.Models;

namespace test
{
    public class MovieRepositoryTest : IClassFixture<DatabaseFixture>                                                                  
    {
        private readonly IMovieRepository _repo;
        DatabaseFixture _fixture;
        public MovieRepositoryTest(DatabaseFixture fixture)
        {
            _fixture = fixture;
            _repo = new MovieRepository(_fixture.dbcontext);
        }

        [Fact]
        public void GetAllMovies()
        {
            //Act
            var actual = _repo.GetAllMovies();
            Assert.IsAssignableFrom<List<Movie>>(actual);
            Assert.NotNull(actual);
            Assert.True(actual.Count>0  );
        }
        [Fact]
        public void AddNewMovie()
        {
            var newMovie = new Movie { Id = 354443, Name = "AntMan", PosterPath = "Antman.jpg", ReleaseDate = "12-10-2012", Comments = String.Empty, VoteAverage = 7.8, VoteCount = 980 };
            var result = _repo.AddNewMovie(newMovie);
            var fullMovies = _repo.GetAllMovies();
            Assert.True(result ==1);
            Assert.Equal(4, fullMovies.Count);

        }
        [Fact]
        public void UpdateMovieDetails()
        {
            var updated = _repo.UpdateMovieDetails(354440, "Hello Superman");
            var movie = _repo.GetAllMovies();

            Assert.True("Hello Superman" == movie.Find(x=>x.Id == 354440).Comments);
        }
        [Fact]
        public void DeleteMovieDetails()
        {
            var updated = _repo.DeleteMovieDetails(354440);
                        
            Assert.True(updated == 1);            
        }
        [Fact]
        public void GetSingleMovieInfo()
        {
            var movie = new Movie { Id = 354440, Name = "Superman", PosterPath = "superman.jpg", ReleaseDate = "12-10-2012", Comments = String.Empty, VoteAverage = 7.8, VoteCount = 980 };
            var actual = _repo.GetSingleMovieInfo(354440);

            Assert.IsAssignableFrom<Movie>(actual);
            Assert.Equal(movie.Name, actual.Name);
        }
        [Fact]
        public void negative_GetAllMovies()
        {
            //Act
            List<Movie> actual = new List<Movie>();
            Assert.IsAssignableFrom<List<Movie>>(actual);
            Assert.True(actual.Count == 0);
        }
        [Fact]
        public void negative_AddNewMovie()
        {
            var newMovie = new Movie();
           Assert.Throws<ArgumentNullException>(() => _repo.AddNewMovie(null));           
            
            
        }
        [Fact]
        public void negative_UpdateMovieDetails()
        {
            
            Assert.Throws<InvalidOperationException>(() => _repo.UpdateMovieDetails(354411140, "Hello Superman"));
            
        }
        [Fact]
        public void negative_DeleteMovieDetails()
        {
            Assert.Throws<ArgumentNullException>(() => _repo.DeleteMovieDetails(12345));
        }
        [Fact]
        public void negative_GetSingleMovieInfo()
        {
            var movie = new Movie { Id = 354440, Name = "", PosterPath = "superman.jpg", ReleaseDate = "12-10-2012", Comments = String.Empty, VoteAverage = 7.8, VoteCount = 980 };
            var actual = _repo.GetSingleMovieInfo(354440);

            Assert.IsAssignableFrom<Movie>(actual);
            Assert.True(movie.Name!= actual.Name);
        }
    }
}
