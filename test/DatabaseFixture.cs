﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using moviecruiser.Data.Models;
using moviecruiser.Data.Persistence;

namespace test
{
    public class DatabaseFixture:IDisposable
    {
        private IEnumerable<Movie> Movies { get; set;}
        public IMoviesDbContext dbcontext;

        public DatabaseFixture()
        {
            var options = new DbContextOptionsBuilder<MoviesDbContext>()
                .UseInMemoryDatabase(databaseName: "MovieDB")
                .Options;
            dbcontext = new MoviesDbContext(options);
            dbcontext.Movies.Add(new Movie { Id = 354440, Name = "Superman", PosterPath = "superman.jpg", ReleaseDate = "12-10-2012", Comments = String.Empty, VoteAverage = 7.8, VoteCount = 980 });
            dbcontext.Movies.Add(new Movie { Id = 354441, Name = "Anaconda", PosterPath = "anaconda.jpg", ReleaseDate = "12-10-2012", Comments = String.Empty, VoteAverage = 8.0, VoteCount = 1080 });
            dbcontext.Movies.Add(new Movie { Id = 354442, Name = "Independence Day", PosterPath = "spiderman.jpg", ReleaseDate = "12-10-2012", Comments = String.Empty, VoteAverage = 7.8, VoteCount = 980 });
            dbcontext.SaveChanges();
        }

        public void Dispose()
        {
            Movies = null;
            dbcontext = null;
        }
    }
}
