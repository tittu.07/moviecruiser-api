﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using moviecruiser.Controllers;
using moviecruiser.Data.Models;
using moviecruiser.Data.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace test
{
    public class MovieControllerTest
    {
        //private readonly IMovieService _service;

        public MovieControllerTest()
        {

        }

        #region Positive test case
        [Fact]
        public void GetMethodWithoutParameter_ShouldRerturnOfMovie()
        {
            //Arrange
            var mockService = new Mock<IMovieService>();
            mockService.Setup(service => service.GetAllMovies()).Returns(GetMovies());
            var controller = new MovieController(mockService.Object);


            //Act
            var result = controller.GetAllMovies();

            //Assert
            var actionResult = Assert.IsType<OkObjectResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<Movie>>(actionResult.Value);
        }
        [Fact]
        public void PostMethod_ShouldInsertNewMovie()
        {
            var newMovie = new Movie { Id = 123456789, Name = "AntMan", PosterPath = "Antman.jpg", ReleaseDate = "12-10-2012", Comments = String.Empty, VoteAverage = 7.8, VoteCount = 980 };

            //Arrange
            var mockService = new Mock<IMovieService>();
            mockService.Setup(service => service.AddNewMovie(newMovie)).Returns(1);
            mockService.Setup(service => service.GetAllMovies()).Returns(GetMovies);
            var controller = new MovieController(mockService.Object);


            //Act
            var result = controller.AddNewMovie(newMovie);

            //Assert
            var actionResult = Assert.IsType<OkObjectResult>(result);
            var model = Assert.IsAssignableFrom<int>(actionResult.Value);
            Assert.True(model == 1);

        }
        [Fact]
        public void PutMethod_ShouldUpdateMovieComment()
        {
            //Arrange
            var mockService = new Mock<IMovieService>();
            mockService.Setup(service => service.UpdateMovieDetails(354440, "Hello Superman")).Returns(1);
            var controller = new MovieController(mockService.Object);


            //Act
            var k = new Movie { Id = 354440, Name = "Superman", PosterPath = "superman.jpg", ReleaseDate = "12-10-2012", Comments = String.Empty, VoteAverage = 7.8, VoteCount = 980 };
            var result = controller.UpdateMovieDetails(k);

            //Assert
            var actionResult = Assert.IsType<OkObjectResult>(result);
            var model = Assert.IsAssignableFrom<int>(actionResult.Value);
            Assert.True(model == 0);
        }
        [Fact]
        public void DeleteMethod_SholuDeleteMovieDetails()
        {
            Movie movie = new Movie { Id = 354440 };
            //Arrange
            var mockService = new Mock<IMovieService>();
            mockService.Setup(service => service.DeleteMovie(354440)).Returns(1);
            var controller = new MovieController(mockService.Object);


            //Act
            var result = controller.DeleteMovie(movie);

            //Assert
            var actionResult = Assert.IsType<OkObjectResult>(result);
            var model = Assert.IsAssignableFrom<int>(actionResult.Value);
            Assert.True(model == 1);
        }
        #endregion

        #region Negative test cases
        [Fact]
        public void GetMethodWithoutParameter_ShouldreturnEmptyList()
        {
            //Arrange
            List<Movie> movies = new List<Movie>();
            var mockService = new Mock<IMovieService>();
            mockService.Setup(service => service.GetAllMovies()).Returns(movies);
            var controller = new MovieController(mockService.Object);


            //Act
            var result = controller.GetAllMovies();

            //Assert
            var actionResult = Assert.IsType<OkObjectResult>(result);
            //   var model = Assert.IsAssignableFrom<IEnumerable<Movie>>(actionResult.Value);
        }

        [Fact]
        public void PostMethod_ShouldThrowError()
        {
            var newMovie = new Movie { Id = 354443, Name = "AntMan", PosterPath = "Antman.jpg", ReleaseDate = "12-10-2012", Comments = String.Empty, VoteAverage = 7.8, VoteCount = 980 };

            //Arrange
            var mockService = new Mock<IMovieService>();
            mockService.Setup(service => service.AddNewMovie(newMovie)).Returns(1);
            var controller = new MovieController(mockService.Object);

            //Assert
            Assert.Throws<ArgumentNullException>(() => controller.AddNewMovie(null));

        }

        [Fact]
        public void PutMethod_ShouldThrowException_UpdateMovieComment()
        {
            //Arrange
            var mockService = new Mock<IMovieService>();
            mockService.Setup(service => service.UpdateMovieDetails(354440, "Hello Superman")).Returns(1);
            var controller = new MovieController(mockService.Object);


            //Assert
            Assert.Throws<NullReferenceException>(() => controller.UpdateMovieDetails(null));


        }

        [Fact]
        public void DeleteMethod_SholudThrowException_DeleteMovieDetails()
        {
            Movie movie = new Movie { Id = 354440 };
            //Arrange
            var mockService = new Mock<IMovieService>();
            mockService.Setup(service => service.DeleteMovie(354440)).Returns(1);
            var controller = new MovieController(mockService.Object);


            //Assert
            Assert.Throws<NullReferenceException>(() => controller.DeleteMovie(null));

        }

        [Fact]
        public void GetMethod_returnEmpty_GetSingleMovieInfo()
        {
            Movie movie = new Movie();
            //Arrange
            var mockService = new Mock<IMovieService>();
            mockService.Setup(service => service.GetSingleMovieInfo(3544401)).Returns(movie.ToString());
            var controller = new MovieController(mockService.Object);


            //Assert
            Assert.Throws<ArgumentNullException>(() => controller.GetSingleMovieInfo(3544401));
          

        }

        #endregion



        private List<Movie> GetMovies()
        {
            var movie = new List<Movie>();
            movie.Add(new Movie { Id = 354440, Name = "Superman", PosterPath = "superman.jpg", ReleaseDate = "12-10-2012", Comments = String.Empty, VoteAverage = 7.8, VoteCount = 980 });
            movie.Add(new Movie { Id = 354441, Name = "Anaconda", PosterPath = "anaconda.jpg", ReleaseDate = "12-10-2012", Comments = String.Empty, VoteAverage = 8.0, VoteCount = 1080 });
            movie.Add(new Movie { Id = 354442, Name = "Independence Day", PosterPath = "spiderman.jpg", ReleaseDate = "12-10-2012", Comments = String.Empty, VoteAverage = 7.8, VoteCount = 980 });
                      
            return movie;
        }
    }
}
